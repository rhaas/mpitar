/* Copyright (c) 2017 The Board of Trustees of the University of Illinois
 * All rights reserved.
 *
 * Developed by: National Center for Supercomputing Applications
 *               University of Illinois at Urbana-Champaign
 *               http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal with the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimers.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimers in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the names of the National Center for Supercomputing Applications,
 * University of Illinois at Urbana-Champaign, nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * WITH THE SOFTWARE.  */

// a very simple proof-of-concept MPI parallelized tar file writer
// The algorithm is fairly straigtforward.
// * the master reads filenames one at a time from stdin then obtains the file
//   size for each file and computes the offset that this file should appear in
//   the tar file based on the previous files
// * it assigns the file to one of the workers who read the full file and
//   places it at the given offset
// * the master bunches up files in lots of 100 or 1e6 bytes of data (whichever
//   is reached first) in the jobs
// * op to 3 jobs are send to a given client at once, clients ack jobs when
//   they are done with it and this triggers a new job to be send to them
// * currently it supports regular files, directories and symbolic links and
//   the output is identical to a regular tar as long as the same file list is
//   passed to tar's -T option

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

#include<mpi.h>

#include<vector>
#include<queue>
#include<string>

//#define DO_TIMING
#include "async_writer.hh"
#include "cmdline.hh"
#include "fileentry.hh"
#include "timer.hh"
#include "tarentry.hh"

#define MAX_JOBS_IN_FLIGHT 3
#define MAX_FILES_IN_JOB 100
#define TARGET_JOB_SIZE (1024ul*1024ul*1024ul)
#define RING_BUFFER_SIZE (512ul*1024ul*1024ul)
// the only file that the master writes is the index file, so the buffer does
// not have to be large
#define MASTER_RING_BUFFER_SIZE (10ul*1024ul*1024ul)


std::vector<timer*> timer::all_timers;
timer timer_all("all");
timer timer_stat("stat"), timer_open("open");
timer timer_read("read"), timer_seek("seek");
timer timer_write_index("write_index");
timer timer_write_master_write("write_master_write");
timer timer_worker_wait("worker_wait"), timer_master_wait("master_wait");

#define DIM(v) (sizeof(v)/sizeof(v[0]))

static void copy_file_content(async_writer& out_writer, const char *out_fn,
                              const tarentry &ent);

static int find_unused_request(int count, MPI_Request *request);
size_t show_progress(size_t total, size_t chunksize, int show_percent);

void master(const char *out_fn, fileentries& entries);
void worker(const char *out_fn);

// control how chatty we are
bool verbose = false;  // show file names
bool progress = false; // show % progress

int main(int argc, char **argv)
{
  int rc = -1;

  MPI_Init(&argc, &argv);
  timer_all.start(__LINE__);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  cmdline args(argc, argv, rank!=0);
  verbose = args.get_verbose();
  progress = args.get_progress();

  const char action = args.get_action();
  switch(action) {
    case cmdline::ACTION_ERROR:
      rc = 1;
      break;
    case cmdline::ACTION_EXIT:
      rc = 0;
      break;
    case cmdline::ACTION_CREATE:
      if(size < 2) {
        if(rank == 0) {
          fprintf(stderr, "Needs at least 2 mpi ranks.\n");
        }
        rc = 1;
      } else {
        if(rank) {
          worker(args.get_tarfilename().c_str());
        } else {
          master(args.get_tarfilename().c_str(), args.get_fileentries());
        }
        rc = 0;
      }
      break;
    default:
      fprintf(stderr, "Unknown action '%c'\n", action);
      rc = 1;
      break;
  }

  timer_all.stop(__LINE__);

  if(rc == 0)
    timer::print_timers();

  MPI_Finalize();

  return rc;
}

void master(const char *out_fn, fileentries& entries)
{
  timer_open.start(__LINE__);
  int out_fd = open(out_fn, O_WRONLY | O_TRUNC | O_CREAT, 0666);
  if(out_fd == -1) {
    fprintf(stderr, "Could not open '%s' for writing: %s\n", out_fn,
            strerror(errno));
    exit(1);
  }
  timer_open.stop(__LINE__);
  // I need this barrier so that the workers wait opening the file until the
  // master has created it
  MPI_Barrier(MPI_COMM_WORLD);
  // this async_writer is only used to write the final index file, so does not
  // need a large buffer (or need ot be async really but this way I can use
  // copy_file_content)
  async_writer out_writer(out_fd, MASTER_RING_BUFFER_SIZE);

  char idx_fn[1024];
  size_t idx_fn_size = snprintf(idx_fn, sizeof(idx_fn), "%s.idx", out_fn);
  assert(idx_fn_size < sizeof(idx_fn));
  timer_open.start(__LINE__);
  FILE *idx_fh = fopen(idx_fn, "w");
  if(idx_fh == NULL) {
    fprintf(stderr, "Could not open '%s' for writing: %s\n", idx_fn,
            strerror(errno));
    exit(1);
  }
  timer_open.stop(__LINE__);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  std::vector<int> jobs_in_flight((size_t)size);
  std::vector<MPI_Request> recv_requests(MAX_JOBS_IN_FLIGHT*size_t(size), MPI_REQUEST_NULL);
  std::vector<int> recv_completed(recv_requests.size());
  std::vector<unsigned long long int> recv_buffers(recv_requests.size());
  std::vector<MPI_Request> send_requests(recv_requests.size(), MPI_REQUEST_NULL);
  std::vector<std::string> send_buffers(recv_requests.size());

  size_t off = 0;
  int done = 0;
  do {
    for(int current_worker = 1 ; current_worker < size && !done ; current_worker++) {
      /* process some MPI requests every once in a while to keep things going */

      /* kick the send requests every once in a while in case MPI does not
       * process them otherwise */
      int dummy1, dummy2;
      timer_master_wait.start(__LINE__);
      MPI_Testany((int)send_requests.size(), &send_requests[0], &dummy1, &dummy2,
                  MPI_STATUSES_IGNORE);

      /* check for ack's of workers */
      int count;
      std::vector<MPI_Status> status(recv_requests.size());
      MPI_Testsome((int)recv_requests.size(), &recv_requests[0], &count,
                   &recv_completed[0], &status[0]);
      timer_master_wait.stop(__LINE__);
      for(int r = 0 ; count != MPI_UNDEFINED && r < count ; r++) {
        int idx = recv_completed[r];
        show_progress(off, size_t(recv_buffers[idx]), 0);

        int w = status[r].MPI_SOURCE;
        jobs_in_flight[w] -= 1;
      }

      /* if the curent worker is underworked, give it something to do */
      if(jobs_in_flight[current_worker] < MAX_JOBS_IN_FLIGHT) {
        const int buf0num = current_worker*MAX_JOBS_IN_FLIGHT;
        int recv_id = find_unused_request(MAX_JOBS_IN_FLIGHT, &recv_requests[buf0num]);
        assert(recv_id >= 0);
        int send_id = find_unused_request(MAX_JOBS_IN_FLIGHT,
                                          &send_requests[buf0num]);
        assert(send_id >= 0);
        send_buffers[buf0num+send_id].clear();
        assert(send_buffers[buf0num+send_id].size() == 0);

        /* make a job package for a worker containing up to MAX_FILES_IN_JOB
         * files and aiming to be at least TARGET_JOB_SIZE bytes worth of files
         * */
        for(size_t n = 0, job_sz = 0 ;
            n < MAX_FILES_IN_JOB &&
              job_sz < TARGET_JOB_SIZE &&
              !done ;
            n++) {
          const std::string fn(entries.nextfile());
          if(fn.empty()) {
            done = 1;
            break;
          }
          if(verbose)
            printf("%s\n", fn.c_str());

          timer_stat.start(__LINE__);
          tarentry ent(fn, off);
          timer_stat.stop(__LINE__);

          const size_t sz = ent.size();
          job_sz += sz;
          //printf("%s (%zu bytes)\n", fn, sz);

          send_buffers.at(buf0num+send_id) += ent.serialize();

          timer_write_index.start(__LINE__);
          fprintf(idx_fh, "%zu %s\n", off, fn.c_str());
          timer_write_index.stop(__LINE__);

          off += sz;
        }
        /* send package to worker */
        if(!send_buffers[buf0num+send_id].empty()) {
          /* prepare for "done" message from worker */
          timer_master_wait.start(__LINE__);
          MPI_Irecv(&recv_buffers[buf0num+recv_id], 1, MPI_UNSIGNED_LONG_LONG, current_worker,
                    send_id, MPI_COMM_WORLD, &recv_requests[buf0num+recv_id]);
          MPI_Isend(send_buffers.at(buf0num+send_id).data(),
                    (int)send_buffers.at(buf0num+send_id).size(), MPI_BYTE,
                    current_worker, send_id, MPI_COMM_WORLD,
                    &send_requests[buf0num+send_id]);
          timer_master_wait.stop(__LINE__);

          jobs_in_flight[current_worker] += 1;
        }
      }
    }
  } while(!done);
  if(progress)
    printf("\rAll done in master, waiting for workers\n");

  /* done, wait for everything to settle down */
  timer_master_wait.start(__LINE__);
  MPI_Waitall((int)send_requests.size(), &send_requests[0], MPI_STATUSES_IGNORE);
  MPI_Waitall((int)recv_requests.size(), &recv_requests[0], MPI_STATUSES_IGNORE);

  if(progress)
    printf("All communication finished in master\n");
  timer_master_wait.stop(__LINE__);

  /* tell all workers to quit */
  for(int current_worker = 1 ; current_worker < size ; current_worker++) {
    /* the empty file name is magic and tells the worker to quit */
    std::string terminate = tarentry().serialize();
    timer_master_wait.start(__LINE__);
    MPI_Send(terminate.c_str(), (int)terminate.size(), MPI_BYTE, current_worker,
             0, MPI_COMM_WORLD);
    timer_master_wait.stop(__LINE__);
  }

  if(progress)
    printf("Closing file.\n");

  timer_master_wait.start(__LINE__);
  MPI_Barrier(MPI_COMM_WORLD);
  timer_master_wait.stop(__LINE__);
  size_t current = show_progress(off, size_t(0), 0);
  show_progress(off, off-current, 0); /* show 100% written */
  if(progress)
    printf("\n");

  /* add index file to tarball */
  timer_write_index.start(__LINE__);
  fprintf(idx_fh, "%zu %s\n", off, idx_fn);
  int ierr_fclose = fclose(idx_fh);
  if(ierr_fclose != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", idx_fn,
            strerror(errno));
    exit(1);
  }
  timer_write_index.stop(__LINE__);

  timer_stat.start(__LINE__);
  tarentry idx_ent(idx_fn, off);
  timer_stat.stop(__LINE__);
  copy_file_content(out_writer, out_fn, idx_ent);
  off += idx_ent.size();

  /* terminate tar file */
  static char buffer[2*BLOCKSIZE];
  int ierr_seek = out_writer.seek(off);
  if(ierr_seek != 0) {
    fprintf(stderr, "Could not seek in '%s': %s\n", out_fn,
            strerror(ierr_seek));
    exit(1);
  }
  timer_write_master_write.start(__LINE__);
  int ierr_write = out_writer.write(buffer, 2*BLOCKSIZE);
  if(ierr_write != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(ierr_write));
    exit(1);
  }
  timer_write_master_write.stop(__LINE__);
  int ierr_async_close = out_writer.close();
  if(ierr_async_close != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(ierr_async_close));
    exit(1);
  }

  timer_write_master_write.start(__LINE__);
  int ierr_close = close(out_fd);
  timer_write_master_write.stop(__LINE__);
  if(ierr_close != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(errno));
    exit(1);
  }

  if(progress)
    printf("Done.\n");

  timer_master_wait.start(__LINE__);
  MPI_Barrier(MPI_COMM_WORLD);
  timer_master_wait.stop(__LINE__);
}

/* make this a non-local type to make the compiler happy */
struct filedesc_t  {
  tarentry ent;
  bool ask_for_work;
  int tag;
  filedesc_t(tarentry ent_, bool ask_for_work_, int tag_) :
    ent(ent_), ask_for_work(ask_for_work_), tag(tag_) {};
};
void worker(const char *out_fn)
{
  std::queue<filedesc_t> files;
  int file_count = 0;

  // open output file
  timer_open.start(__LINE__);
  // I need this barrier so that worker ranks wait until the master has created
  // the file
  MPI_Barrier(MPI_COMM_WORLD);
  int out_fd = open(out_fn, O_WRONLY, 0666);
  if(out_fd == -1) {
    fprintf(stderr, "Could not open '%s' for writing: %s\n", out_fn,
            strerror(errno));
    exit(1);
  }
  timer_open.stop(__LINE__);
  async_writer out_writer(out_fd, RING_BUFFER_SIZE);

  int done = 0;
  do {
    /* check for any new work package from master, wait only if we have nothing
     * else to do */
    MPI_Status status;
    int count, flag;
    if(files.empty()) {
      timer_worker_wait.start(__LINE__);
      MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      timer_worker_wait.stop(__LINE__);
      flag = 1;
    } else {
      timer_worker_wait.start(__LINE__);
      MPI_Iprobe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
      timer_worker_wait.stop(__LINE__);
    }
    if(flag) {
      /* data is waiting, get it */
      timer_worker_wait.start(__LINE__);
      MPI_Get_count(&status, MPI_BYTE, &count);
      std::vector<char> recv_buffer(count);
      int tag = status.MPI_TAG;
      MPI_Recv(&recv_buffer[0], count, MPI_BYTE, 0, MPI_ANY_TAG, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
      timer_worker_wait.stop(__LINE__);

      /* parse buffer into file entries */
      for(size_t ind = 0, sz ; ind < recv_buffer.size() ; ind += sz) {
        tarentry ent;
        sz = ent.deserialize(&recv_buffer[ind]);
        /* magic empty file name for end of work? */
        if(ent.get_filename().empty()) {
          done = 1;
          break;
        }
        /* request more work from master when processing first entry */
        files.push(filedesc_t(ent, ind==0, tag));
      }
    }

    /* only do one file, then look for more work from master, this assumes that
     * MPI is much faster than IO */
    if(!files.empty()) {
      assert(sizeof(size_t) <= sizeof(unsigned long long int));
      static unsigned long long int chunk_written = 0;
      const filedesc_t& file = files.front();
      copy_file_content(out_writer, out_fn, file.ent);
      file_count += 1;
      chunk_written += static_cast<unsigned long long int>(file.ent.size());

      if(file.ask_for_work) {
        timer_worker_wait.start(__LINE__);
        MPI_Send(&chunk_written, 1, MPI_UNSIGNED_LONG_LONG, 0, file.tag, MPI_COMM_WORLD);
        timer_worker_wait.stop(__LINE__);
        chunk_written = 0;
      }
      files.pop();
    }

  } while(!done || !files.empty());

  /* this will usually induce a delay while caches are flushed */
  int ierr_async_close = out_writer.close();
  if(ierr_async_close != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(ierr_async_close));
    exit(1);
  }
  timer_write_master_write.start(__LINE__);
  int ierr_close = close(out_fd);
  timer_write_master_write.stop(__LINE__);
  if(ierr_close != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(errno));
    exit(1);
  }
  // the barrier before master reports 100% done
  timer_worker_wait.start(__LINE__);
  MPI_Barrier(MPI_COMM_WORLD);
  timer_worker_wait.stop(__LINE__);
  // the barrier at the end of master
  timer_worker_wait.start(__LINE__);
  MPI_Barrier(MPI_COMM_WORLD);
  timer_worker_wait.stop(__LINE__);
}

static int find_unused_request(int count, MPI_Request *request)
{
  int flag, idx;
  
  for(int r = 0 ; r < count ; r++) {
    if(request[r] == MPI_REQUEST_NULL) {
      return r;
    }
  }

  timer_master_wait.start(__LINE__);
  MPI_Testany(count, request, &idx, &flag, MPI_STATUSES_IGNORE);
  timer_master_wait.stop(__LINE__);
  return flag ? idx : -1;
}

static void copy_file_content(async_writer& out_writer, const char *out_fn,
                              const tarentry &ent)
{
  const size_t off = ent.get_offset();
  const char *in_fn = ent.get_filename().c_str();
  const std::vector<char> hdr(ent.make_tar_header());

  // seek only when required to avoid flushes
  // ftell however seems to call fflush() so we keep track of the file pointer
  // ourselves
  static size_t file_off = 0;
  timer_seek.start(__LINE__);
  if(file_off != off) {
    int ierr_seek = out_writer.seek(off);
    if(ierr_seek != 0) {
      fprintf(stderr, "Could not seek in '%s': %s\n", out_fn,
              strerror(ierr_seek));
      exit(1);
    }
    file_off = off;
  }
  timer_seek.stop(__LINE__);

  void *data = malloc(hdr.size());
  assert(data);
  memcpy(data, static_cast<const void*>(hdr.data()), hdr.size());
  int ierr_write_hdr = out_writer.write(data, hdr.size());
  if(ierr_write_hdr != 0) {
    fprintf(stderr, "Could not write to '%s': %s\n", out_fn,
            strerror(ierr_write_hdr));
    exit(1);
  }
  file_off += hdr.size();
  if(!ent.is_reg())
    return;

  timer_open.start(__LINE__);
  int in_fd = open(in_fn, O_RDONLY);
  timer_open.stop(__LINE__);
  if(in_fd < 0) {
    fprintf(stderr, "Could not open '%s' for reading: %s\n", in_fn,
            strerror(errno));
    exit(1);
  }
  off_t size = (off_t)ent.get_filesize();
  int ierr_copy = out_writer.copy(in_fd, size);
  if(ierr_copy) {
    fprintf(stderr, "Could not copy '%s': %s\n", in_fn, strerror(ierr_copy));
    exit(1);
  }
  file_off += size;

  static char block[BLOCKSIZE]; /* bunch of zeros for padding to block size */
  if(size % BLOCKSIZE) {
    const size_t padsize = BLOCKSIZE - (size % BLOCKSIZE);
    int ierr_write_tail = out_writer.write(block, padsize);
    if(ierr_write_tail) {
      fprintf(stderr, "Could not write to '%s': %s\n", out_fn, strerror(ierr_write_tail));
      exit(1);
    }
    file_off += padsize;
  }
  timer_open.start(__LINE__);
  int ierr_close = close(in_fd);
  assert(ierr_close == 0);
  timer_open.stop(__LINE__);

}

size_t show_progress(size_t total, size_t chunksize, int show_percent)
{
  static size_t written = 0;

  written += chunksize;
  if(progress) {
    static const char *units[] = {"B", "kB", "MB", "GB", "TB", "PB"};
    int unit = int(written > 0 ? log2(written)/10 : 0);
    if(unit >= int(DIM(units)))
      unit = DIM(units) - 1;
    printf("\r% 6.1f %s done", written/pow(2.,10*unit), units[unit]);
    if(show_percent)
      printf(" (% 4.1f%%)", 100.*((double)written)/total);
    fflush(stdout);
  }

  return written;
}
