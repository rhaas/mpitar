/* Copyright (c) 2017 The Board of Trustees of the University of Illinois
 * All rights reserved.
 *
 * Developed by: National Center for Supercomputing Applications
 *               University of Illinois at Urbana-Champaign
 *               http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal with the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimers.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimers in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the names of the National Center for Supercomputing Applications,
 * University of Illinois at Urbana-Champaign, nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * WITH THE SOFTWARE.  */

//#define DO_TIMING
#include "async_writer.hh"
#include "timer.hh"

#include <algorithm>

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <limits.h>

#define DIM(v) (sizeof(v)/sizeof(v[0]))

timer timer_write_worker_wait("write_worker_wait");
timer timer_write_worker_write("write_worker_write");
timer timer_write_master_wait("write_master_wait");

async_writer::async_writer(const int out_fd_, const size_t capacity_) :
   out_fd(out_fd_), error_code(0), ring(capacity_), floor(0), height(0),
   active_seeks(0), thread_active(false), do_exit(false)
{
  int ring_mutex_init = pthread_mutex_init(&ring_lock, NULL);
  int ring_cond_init = pthread_cond_init(&ring_wait, NULL);
  if(ring_mutex_init != 0 || ring_cond_init != 0) {
    fprintf(stderr, "Failed to initialize mutexes for ring variable: %d %d\n",
            ring_mutex_init, ring_cond_init);
    exit(1);
  }

  int thread_create = pthread_create(&writer_thread, NULL, writer_func,
                                       static_cast<void*>(this));
  if(thread_create != 0) {
    fprintf(stderr, "Failed to create writer thread: %d\n", thread_create);
    exit(1);
  }
  thread_active = true;
};

async_writer::~async_writer()
{
  close();
  assert(!thread_active);
}

int async_writer::write(const void* buf, size_t count)
{
  assert(thread_active);

  int my_error_code = 0;

  const unsigned char *p = static_cast<const unsigned char *>(buf);
  while(count > 0) {
    timer_write_master_wait.start(__LINE__);
    pthread_mutex_lock(&ring_lock);
    while(height >= ring.size() && error_code == 0)
      pthread_cond_wait(&ring_wait, &ring_lock);
    timer_write_master_wait.stop(__LINE__);

    size_t chunk_count = std::min(count, ring.size() - height);
    size_t chunk_start = floor + height;
    if(chunk_start >= ring.size())
      chunk_start -= ring.size();
    // assert() that even wrapping around we are not writing in between floor
    // and floor+height
    assert(chunk_start + chunk_count <= floor + ring.size());
    my_error_code = error_code;

    pthread_cond_signal(&ring_wait);
    pthread_mutex_unlock(&ring_lock);

    if(my_error_code)
      break;

    if(chunk_start + chunk_count > ring.size()) { // wraps around
      memcpy(&ring[chunk_start], p, ring.size() - chunk_start);
      p += ring.size() - chunk_start;
      memcpy(&ring[0], p, chunk_start + chunk_count - ring.size());
      p += chunk_start + chunk_count - ring.size();
    } else { // add in one go
      memcpy(&ring[chunk_start], p, chunk_count);
      p += chunk_count;
    }
    count -= chunk_count;

    timer_write_master_wait.start(__LINE__);
    pthread_mutex_lock(&ring_lock);
    timer_write_master_wait.stop(__LINE__);

    height += chunk_count;
    assert(height <= ring.size());

    pthread_cond_signal(&ring_wait);
    pthread_mutex_unlock(&ring_lock);
  }
  assert(my_error_code == 0 || count == 0);

  return my_error_code;
}

int async_writer::copy(const int in_fd, size_t count)
{
  assert(thread_active);

  int my_error_code = 0;

  while(count > 0 && my_error_code == 0) {
    timer_write_master_wait.start(__LINE__);
    pthread_mutex_lock(&ring_lock);
    while(height >= ring.size() && error_code == 0)
      pthread_cond_wait(&ring_wait, &ring_lock);
    timer_write_master_wait.stop(__LINE__);

    size_t chunk_count = std::min(count, ring.size() - height);
    size_t chunk_start = floor + height;
    if(chunk_start >= ring.size())
      chunk_start -= ring.size();
    // assert() that even wrapping around we are not writing in between floor
    // and floor+height
    assert(chunk_start + chunk_count <= floor + ring.size());
    my_error_code = error_code;

    pthread_cond_signal(&ring_wait);
    pthread_mutex_unlock(&ring_lock);

    // don't do anything anymore an error has been reported by the writer
    if(my_error_code == 0) {
      assert(IOV_MAX >= 2);
      struct iovec iovecs[2];
      int nvec;
      if(chunk_start + chunk_count > ring.size()) { // wraps around
        nvec = 2;
        iovecs[0].iov_base = &ring[chunk_start];
        iovecs[0].iov_len = ring.size() - chunk_start;
        iovecs[1].iov_base = &ring[0];
        iovecs[1].iov_len = chunk_start + chunk_count - ring.size();
      } else { // can write in single chunk
        nvec = 1;
        iovecs[0].iov_base = &ring[chunk_start];
        iovecs[0].iov_len = chunk_count;
      }

      ssize_t bytes_to_read = chunk_count;
      while(bytes_to_read > 0) {
        ssize_t bytes_read = readv(in_fd, iovecs, nvec);
        if(bytes_read == -1) {
          my_error_code = errno;
          break;
        }
        bytes_to_read -= bytes_read;
        assert(bytes_to_read >= 0);
        // update chunks to read
        for(int i = 0 ; i < nvec ; i++) {
          if((size_t)bytes_read >= iovecs[i].iov_len) {
            bytes_read -= iovecs[i].iov_len;
            iovecs[i].iov_len = 0;
          } else {
            iovecs[i].iov_base =
              static_cast<unsigned char *>(iovecs[i].iov_base) + bytes_read;
            iovecs[i].iov_len -= bytes_read;
            break;
          }
        }
        assert(bytes_read == 0);
      }

      timer_write_master_wait.start(__LINE__);
      pthread_mutex_lock(&ring_lock);
      timer_write_master_wait.stop(__LINE__);

      if(my_error_code == 0) {
        assert(bytes_to_read == 0);
        height += chunk_count;
        assert(height <= ring.size());
      } else if(error_code == 0) {
        error_code = my_error_code;
      }

      pthread_cond_signal(&ring_wait);
      pthread_mutex_unlock(&ring_lock);

      count -= chunk_count;
    }
  }

  assert(my_error_code != 0 || count == 0);
  return my_error_code;
}

int async_writer::seek(const off_t offset)
{
  assert(thread_active);

  int my_error_code = 0;

  // wait for there being room in the seeks queue
  timer_write_master_wait.start(__LINE__);
  pthread_mutex_lock(&ring_lock);
  while(active_seeks >= DIM(seeks) && error_code == 0)
    pthread_cond_wait(&ring_wait, &ring_lock);
  timer_write_master_wait.stop(__LINE__);

  my_error_code = error_code;

  if(my_error_code == 0) {
    assert(active_seeks == 0 ||
           seeks[active_seeks-1].trigger <= floor + height);
    seeks[active_seeks].trigger = floor + height;
    seeks[active_seeks].offset = offset;
    active_seeks += 1;

    // check sanity of seeks
    for(size_t i = 1 ; i < active_seeks ; i++) {
      assert(seeks[i-1].trigger <= seeks[i].trigger);
    }
  }

  pthread_mutex_unlock(&ring_lock);

  return my_error_code;
}

int async_writer::close()
{
  if(!thread_active)
    return 0;

  // wait for buffer to fully drain
  timer_write_master_wait.start(__LINE__);
  pthread_mutex_lock(&ring_lock);
  while(height > 0 && error_code == 0)
    pthread_cond_wait(&ring_wait, &ring_lock);
  timer_write_master_wait.stop(__LINE__);
  do_exit = true;
  pthread_cond_signal(&ring_wait);
  pthread_mutex_unlock(&ring_lock);
  
  // kill writer thread exit
  int join_ierr = pthread_join(writer_thread, NULL);
  if(join_ierr != 0) {
    fprintf(stderr, "Failed to join with writer thread: %d\n", join_ierr);
    exit(1);
  }

  thread_active = false;

  // no other thread anymore, no need for the locks
  return error_code;
}

void* async_writer::writer_func(void* calldata)
{
  async_writer* obj = static_cast<async_writer*>(calldata);
  obj->writer();
  return NULL;
}
  
void async_writer::writer()
{
  int my_error_code = 0;

  while(my_error_code == 0) {
    timer_write_worker_wait.start(__LINE__);
    pthread_mutex_lock(&ring_lock);
    while(height == 0 && error_code == 0 && !do_exit)
      pthread_cond_wait(&ring_wait, &ring_lock);
    timer_write_worker_wait.stop(__LINE__);

    size_t chunk_count = height;
    size_t chunk_start = floor;
    size_t seek_trigger = seeks[0].trigger;
    off_t seek_offset = seeks[0].offset;
    bool have_seek = active_seeks > 0;
    bool my_do_exit = do_exit;
    my_error_code = error_code;

    pthread_mutex_unlock(&ring_lock);

    assert(!have_seek || seek_trigger >= chunk_start);

    if(my_do_exit || my_error_code)
      break;

    if(have_seek && chunk_start == seek_trigger) {
      // Seek if we have hit the trigger. We will always exactly hit the
      // trigger.
      off_t pos = lseek(out_fd, seek_offset, SEEK_SET);
      if(pos == -1)
        my_error_code = errno;
      have_seek = false;

      timer_write_worker_wait.start(__LINE__);
      pthread_mutex_lock(&ring_lock);
      timer_write_worker_wait.stop(__LINE__);

      if(my_error_code == 0) {
        active_seeks -= 1;
        memmove(&seeks[1], &seeks[0], active_seeks * sizeof(seeks[0]));
      } else if(error_code == 0) {
        error_code = my_error_code;
      }

      pthread_cond_signal(&ring_wait);
      pthread_mutex_unlock(&ring_lock);
   } else {
      // no trigger, write some data
      // reduce write to only write up to seek trigger
      if(have_seek && chunk_start + chunk_count > seek_trigger)
          chunk_count = seek_trigger - chunk_start;

      assert(IOV_MAX >= 2);
      struct iovec iovecs[2];
      int nvec;
      if(chunk_start + chunk_count > ring.size()) { // wraps around
        nvec = 2;
        iovecs[0].iov_base = &ring[chunk_start];
        iovecs[0].iov_len = ring.size() - chunk_start;
        iovecs[1].iov_base = &ring[0];
        iovecs[1].iov_len = chunk_start + chunk_count - ring.size();
      } else { // can write in single chunk
        nvec = 1;
        iovecs[0].iov_base = &ring[chunk_start];
        iovecs[0].iov_len = chunk_count;
      }

      ssize_t bytes_to_write = chunk_count;
      while(bytes_to_write > 0) {
        ssize_t bytes_written = writev(out_fd, iovecs, nvec);
        if(bytes_written == -1) {
          my_error_code = errno;
          break;
        }
        bytes_to_write -= bytes_written;
        assert(bytes_to_write >= 0);
        // update chunks to write
        for(int i = 0 ; i < nvec ; i++) {
          if((size_t)bytes_written >= iovecs[i].iov_len) {
            bytes_written -= iovecs[i].iov_len;
            iovecs[i].iov_len = 0;
          } else {
            iovecs[i].iov_base = (char*)iovecs[i].iov_base + bytes_written;
            iovecs[i].iov_len -= bytes_written;
            break;
          }
        }
        assert(bytes_written == 0);
      }

      timer_write_worker_wait.start(__LINE__);
      pthread_mutex_lock(&ring_lock);
      timer_write_worker_wait.stop(__LINE__);

      if(my_error_code == 0) {
        assert(bytes_to_write == 0);
        floor += chunk_count;
        if(floor >= ring.size()) { // wrapround happened
          // need to update all triggers which were above the wraparound point
          for(size_t i = 0 ; i < active_seeks ; i++) {
            assert(seeks[i].trigger >= floor);
            seeks[i].trigger -= ring.size();
            assert(seeks[i].trigger < ring.size());
          }

          floor -= ring.size();
        }
        assert(chunk_count <= height);
        height -= chunk_count;
      }
      pthread_cond_signal(&ring_wait);
      pthread_mutex_unlock(&ring_lock);
    } // if chunk_start == seek_trigger
  } // while my_error_code

  // this publishes any error that I encountered but only if no one else
  // already encountered an error
  if(my_error_code != 0) {
    timer_write_worker_wait.start(__LINE__);
    pthread_mutex_lock(&ring_lock);
    timer_write_worker_wait.stop(__LINE__);

    if(error_code == 0) {
      error_code = my_error_code;
      pthread_cond_signal(&ring_wait);
    }
    pthread_mutex_unlock(&ring_lock);
  }
}
