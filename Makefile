CXX=mpicxx
CXXFLAGS=-Wall -g3 -O2
# we call getgrgid which requires a dynamic executable and SEGFAULTs for a
# statically linked one
export CRAYPE_LINK_TYPE=dynamic
export XTPE_LINK_TYPE=dynamic

SOURCES = mpitar.cc tarentry.cc cmdline.cc async_writer.cc
HEADERS = timer.hh tarentry.hh fileentry.hh cmdline.hh async_writer.hh

.PHONY: clean all

all: mpitar test

mpitar: $(SOURCES) $(HEADERS) Makefile
	$(CXX) $(LDFLAGS) $(CXXFLAGS) $(SOURCES) $(LIBS) -o mpitar

test: mpitar test.sh Makefile
	@rm -rf test
	./test.sh || cat test/test.log

clean:
	rm -f mpitar *~
	rm -rf test
